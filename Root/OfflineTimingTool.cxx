#include <OfflineTimingTool/OfflineTimingTool.h>

//
// Initialize the tool and load the corrections
//____________________________________________
OfflineTimingTool::OfflineTimingTool(bool debug){

  Info( "OfflineTimingTool::OfflineTimingTool()", "Initializing The Offline Precision Timing Tool" );
 
  if( debug ) Info( "OfflineTimingTool::OfflineTimingTool()", "Setting debug to true");
  m_debug = debug;

  if( initialize() != EL::StatusCode::SUCCESS )
    Info( "OfflineTimingTool::OfflineTimingTool()", "Failed to properly initialize the tool!" );

}

//
// Load corrections to apply
//_________________________________________
EL::StatusCode OfflineTimingTool::initialize(){


  // Load resolution Parameters for MC smearing
  Info("OfflineTimingTool::initialize()", Form(">> Loading Resolution Parameters for MC Smearing"));
  std::ifstream fh( PathResolverFindCalibFile( Form("OfflineTimingTool/Resolution/resParamsH.dat")) );
  std::ifstream fm( PathResolverFindCalibFile( Form("OfflineTimingTool/Resolution/resParamsM.dat")) );
  std::ifstream fl( PathResolverFindCalibFile( Form("OfflineTimingTool/Resolution/resParamsL.dat")) );
  // Make sure file opened correctly
  if( !fh.is_open() ){
    Info("OfflineTimingTool::initialize()",Form("!!! Failed to open resParamsH.dat"));
    return EL::StatusCode::FAILURE;
  }
  if( !fm.is_open() ){
    Info("OfflineTimingTool::initialize()",Form("!!! Failed to open resParamsM.dat"));
    return EL::StatusCode::FAILURE;
  }
  if( !fl.is_open() ){
    Info("OfflineTimingTool::initialize()",Form("!!! Failed to open resParamsL.dat"));
    return EL::StatusCode::FAILURE;
  }
  std::string l;
  std::vector< std::string > corr;
  while( std::getline(fh, l) ){
    boost::split( corr, l, boost::is_any_of(" "), boost::token_compress_on);
    resParamsH.push_back( corr );
  }
  fh.close();
  while( std::getline(fm, l) ){
    boost::split( corr, l, boost::is_any_of(" "), boost::token_compress_on);
    resParamsM.push_back( corr );
  }
  fm.close();
  while( std::getline(fl, l) ){
    boost::split( corr, l, boost::is_any_of(" "), boost::token_compress_on);
    resParamsL.push_back( corr );
  }
  fl.close();
  Info( "OfflineTimingTool::initialize()", Form(" ->Successfully Loaded Resolution Parameters, %lu High, %lu Med and %lu Low Corrections",
         resParamsH.size(), resParamsM.size(), resParamsL.size()) );

    // Update this config file
    std::string inputName = PathResolverFindCalibFile( "OfflineTimingTool/RunNumberList.txt");
    std::ifstream f1( inputName );
    // Make sure file opened correctly
    if( !f1.is_open() ){
      Info("OfflineTimingTool::initialize()",Form(" !!! Failed to open RunNumber.txt "));
      return EL::StatusCode::FAILURE;
    }
    std::string inputName_channel = PathResolverFindCalibFile( "OfflineTimingTool/channel.txt");
    std::ifstream f_channel( inputName_channel );
    // Make sure file opened correctly
    if( !f_channel.is_open() ){
      Info("OfflineTimingTool::initialize()",Form(" !!! Failed to open channel.txt "));
      return EL::StatusCode::FAILURE;
    }

    std::string inputName_bad_channel = PathResolverFindCalibFile( "OfflineTimingTool/bad_channel.txt");
    std::ifstream f_bad_channel( inputName_bad_channel );
    // Make sure file opened correctly
    if( !f_bad_channel.is_open() ){
      Info("OfflineTimingTool::initialize()",Form("  !!! Failed to open bad_channel.txt "));
      return EL::StatusCode::FAILURE;
    }

    std::string inputName_iov = PathResolverFindCalibFile( "OfflineTimingTool/iov.txt");
    std::ifstream f_iov( inputName_iov );
    // Make sure file opened correctly
    if( !f_iov.is_open() ){
      Info("OfflineTimingTool::initialize()",Form("  !!! Failed to open iov.txt "));
      return EL::StatusCode::FAILURE;
    }
    //clearvector to avoid multiple initialization
    iovNumberList.clear(); 
    runNumberList.clear();
    channelList_for_subslot.clear();
    bad_channelList.clear();
    p0CorrH.clear();
    p0CorrM.clear();
    p8CorrH.clear();
    p8CorrM.clear();
    p1CorrH.clear();
    p1CorrM.clear();
    p2CorrH.clear();
    p2CorrM.clear();
    p3CorrH.clear();
    p3CorrM.clear();
    p4CorrH.clear();
    p4CorrM.clear();
    p5CorrH.clear();
    p5CorrM.clear();
    p6CorrH.clear();
    p6CorrM.clear();
    p7CorrH.clear();
    p7CorrM.clear();

    int runNum;
                       

    // Store each run into local vector
    while( f1 >> runNum ){
	    runNumberList.push_back(runNum);
    }
    f1.close();
    int chNum;
    channelList_for_subslot.cleasr
//    // Store each run into local vector
    while( f_channel >> chNum ){
      channelList_for_subslot.push_back(chNum);
    }
    f_channel.close();
    int badchNum;
    // Store each run into local vector
    while( f_bad_channel >> badchNum ){
      bad_channelList.push_back(badchNum);
    }
    f_bad_channel.close();
    int iovNum;
    // Store each run into local vector
    while( f_iov >> iovNum ){
      iovNumberList.push_back(iovNum);
    }
    f_iov.close();
    // Print Loaded Run Numbers
    Info( "OfflineTimingTool::initialize()", Form("Total Number of Runs for %d", runNumberList.size()) );
    Info( "OfflineTimingTool::initialize()", Form("Run %d: %d", 0, runNumberList[0]) );
    Info( "OfflineTimingTool::initialize()", Form("Run %d: %d", 0, runNumberList[1]) );
    Info( "OfflineTimingTool::initialize()", Form("Run %lu: %d", runNumberList.size()-1, runNumberList.back() ) );
    Info( "OfflineTimingTool::initialize()", Form("Total Number of Channel for subslot for %d", channelList_for_subslot.size()) );
    Info( "OfflineTimingTool::initialize()", Form("Channel for subslot %d: %d", 0, channelList_for_subslot[0]) );
    Info( "OfflineTimingTool::initialize()", Form("Channel for subslot %d: %d", 0, channelList_for_subslot[1]) );
    Info( "OfflineTimingTool::initialize()", Form("Channel for subslot %lu: %d", channelList_for_subslot.size()-1, channelList_for_subslot.back() ) );
    Info( "OfflineTimingTool::initialize()", Form("Total Number of bad channel for %d", bad_channelList.size()) );
    Info( "OfflineTimingTool::initialize()", Form("Bad channel %d: %d", 0, bad_channelList[0]) );
    Info( "OfflineTimingTool::initialize()", Form("Bad channel %lu: %d", bad_channelList.size()-1, bad_channelList.back() ) );
    Info( "OfflineTimingTool::initialize()", Form("Total Number of iov for %d", iovNumberList.size()) );
    Info( "OfflineTimingTool::initialize()", Form("iov %d: %d", 0, iovNumberList[0]) );
    Info( "OfflineTimingTool::initialize()", Form("iov %d: %d", 0, iovNumberList[1]) );
    Info( "OfflineTimingTool::initialize()", Form("iov %lu: %d", iovNumberList.size()-1, iovNumberList.back() ) );

    // Correction Files to read
    std::string configHigh[9]   = {"OfflineTimingTool/runByRunFTOffsetsH.dat", 
        "OfflineTimingTool/AvgFebOffsetsH.dat",     
        "OfflineTimingTool/AvgChOffsetsH.dat",      
        "OfflineTimingTool/AvgEnSlFitH.dat",        
        "OfflineTimingTool/AvgdPhidEtaSlFitH.dat",  
        "OfflineTimingTool/Avgf1f3SlFitH.dat",      
        "OfflineTimingTool/AvgBunchOffsets0.dat",         
        "OfflineTimingTool/AvgChOffsetsp6H.dat",        
        "OfflineTimingTool/runByRunFTOffsetsH_2nd.dat"};
    std::string configMedium[9] = {"OfflineTimingTool/runByRunFTOffsetsM.dat", 
        "OfflineTimingTool/AvgFebOffsetsM.dat",     
        "OfflineTimingTool/AvgChOffsetsM.dat",      
        "OfflineTimingTool/AvgEnSlFitM.dat",        
        "OfflineTimingTool/AvgdPhidEtaSlFitM.dat",  
        "OfflineTimingTool/Avgf1f3SlFitM.dat",      
        "OfflineTimingTool/AvgBunchOffsets1.dat",         
        "OfflineTimingTool/AvgChOffsetsp6M.dat",        
        "OfflineTimingTool/runByRunFTOffsetsM_2nd.dat"};

    // Loop over each correction file and load to local vector
    for( int nCorr = 0; nCorr <= 8; nCorr++){
      // high and medium gain files
      std::ifstream f2( PathResolverFindCalibFile( configHigh[nCorr].c_str() ) );
      std::ifstream f3( PathResolverFindCalibFile( configMedium[nCorr].c_str() ) );
      // Make sure opened correctly
      if( !f2.is_open() ){
        Info( "OfflineTimingTool::initialize()", Form("!!! Failed to open: %s",configHigh[nCorr].c_str()));
        return EL::StatusCode::FAILURE;
      }
      if( !f3.is_open() ){
        Info("OfflineTimingTool::initialize()", Form("!!! Failed to open: %s",configMedium[nCorr].c_str() ));
        return EL::StatusCode::FAILURE;
      }
      // to read each line and separate the corrections
      std::string line;
      std::vector< std::string > corrections;
      // High gain corrections
      while( std::getline(f2, line) ){
        boost::split( corrections, line, boost::is_any_of(" "), boost::token_compress_on);
        switch( nCorr){
          case 0: 
            p0CorrH.push_back(corrections); break;
          case 1:
            p1CorrH.push_back(corrections[1]); break;
          case 2:
            p2CorrH.push_back(corrections); break;
          case 3:
            p3CorrH.push_back(corrections); break;
          case 4:
            p4CorrH.push_back(corrections); break;
          case 5:
            p5CorrH.push_back(corrections); break;
          case 6:
            p6CorrH.push_back(corrections); break;
          case 7:
            p7CorrH.push_back(corrections[1]); break;
          case 8:
            p8CorrH.push_back(corrections); break;
        }
      } // end loading high gain
      f2.close();
      // Medium gain corrections
      while( std::getline(f3, line) ){
        boost::split( corrections, line, boost::is_any_of(" "), boost::token_compress_on);
        switch( nCorr){
          case 0: 
            p0CorrM.push_back(corrections); break;
          case 1:
            p1CorrM.push_back(corrections[1]); break;
          case 2:
            p2CorrM.push_back(corrections); break;
          case 3:
            p3CorrM.push_back(corrections); break;
          case 4:
            p4CorrM.push_back(corrections); break;
          case 5:
            p5CorrM.push_back(corrections); break;
          case 6:
            p6CorrM.push_back(corrections); break;
          case 7:
            p7CorrM.push_back(corrections[1]); break;
          case 8:
            p8CorrM.push_back(corrections); break;
        }
      } // end loading medium gain
      f3.close();
      // Print how many corrections loaded
      switch( nCorr ){
        case 0:
          Info( "OfflineTimingTool::initialize()", Form(" ->Successfully Loaded Pass %d corrections, %lu High and %lu Med Corrections",
                 nCorr, p0CorrH.size(), p0CorrM.size()) ); break;
        case 1:
          Info( "OfflineTimingTool::initialize()", Form(" ->Successfully Loaded Pass %d corrections, %lu High and %lu Med Corrections",
                 nCorr, p1CorrH.size(), p1CorrM.size()) ); break;
        case 2:
          Info( "OfflineTimingTool::initialize()", Form(" ->Successfully Loaded Pass %d corrections, %lu High and %lu Med Corrections",
                 nCorr, p2CorrH.size(), p2CorrM.size()) ); 
	  if(!m_debug)  break;
	Info( "OfflineTimingTool::initialize()", Form("Pass 2 correction High ch[%d] iov[%d]: %s",1009, 2, p2CorrH[1009][2].c_str() ) );
	Info("OfflineTimingTool::initialize()" , Form("Pass 2 correction Med  ch[%d] iov[%d]: %s",1009, 2, p2CorrM[1009][2].c_str() ) );
	Info( "OfflineTimingTool::initialize()", Form("Successfully Loaded Pass %d corrections, %lu High, %lu Med and %lu Low Corrections",
				nCorr,p2CorrH.size(),p2CorrM.size()) );   	break;
        case 3:
          Info( "OfflineTimingTool::initialize()", Form(" ->Successfully Loaded Pass %d corrections, %lu High and %lu Med Corrections",
                 nCorr, p3CorrH.size(), p3CorrM.size()) ); break;
        case 4:
          Info( "OfflineTimingTool::initialize()", Form(" ->Successfully Loaded Pass %d corrections, %lu High and %lu Med Corrections",
                 nCorr, p4CorrH.size(), p4CorrM.size()) ); break;
        case 5:
          Info( "OfflineTimingTool::initialize()", Form(" ->Successfully Loaded Pass %d corrections, %lu High and %lu Med Corrections",
                 nCorr, p5CorrH.size(), p5CorrM.size()) ); break;
        case 6:
          Info( "OfflineTimingTool::initialize()", Form(" ->Successfully Loaded Pass %d corrections, %lu High and %lu Med Corrections",
                 nCorr, p6CorrH.size(), p6CorrM.size()) ); break;
        case 7:
          Info( "OfflineTimingTool::initialize()", Form(" ->Successfully Loaded Pass %d corrections, %lu High and %lu Med Corrections",
                 nCorr, p7CorrH.size(), p7CorrM.size()) ); break;
        case 8:
          Info( "OfflineTimingTool::initialize()", Form(" ->Successfully Loaded Pass %d corrections, %lu High and %lu Med Corrections",
                 nCorr, p8CorrH.size(), p8CorrM.size()) ); break;
      } // end printing loaded constants

    } // end loop over loading configuration files
  return EL::StatusCode::SUCCESS;
}

//
// Find the IOV given runNumber
//____________________________________________________
EL::StatusCode OfflineTimingTool::setIOV(caloObject_t &calObj){

	unsigned int rN = calObj.m_rn;
	unsigned int pos = std::find( runNumberList.begin(), runNumberList.end(), rN) - runNumberList.begin();
	calObj.m_rn_ind=pos;
    if(pos==std::string::npos)
        Info( "OfflineTimingTool::initialize()", Form("This run is not in the GRL of 2016"));

	calObj.m_iov = 4;
	if(pos<iovNumberList[0]) calObj.m_iov=0;
	else if(pos>=iovNumberList[0]&&pos<(iovNumberList[0]+iovNumberList[1])) calObj.m_iov=1;
	else if(pos>=(iovNumberList[0]+iovNumberList[1])&&pos<(iovNumberList[0]+iovNumberList[1]+iovNumberList[2])) calObj.m_iov=2;
	else if(pos>=(iovNumberList[0]+iovNumberList[1]+iovNumberList[2])&&pos<(iovNumberList[0]+iovNumberList[1]+iovNumberList[2]+iovNumberList[3]))calObj.m_iov=3;
	if(m_debug )
	cout<<"rN"<<rN<<"iov"<<calObj.m_iov<<endl;

  return EL::StatusCode::SUCCESS;
}

//
// Calculate and set dPhi/dEta
//____________________________________________________
EL::StatusCode OfflineTimingTool::setdPhidEta(caloObject_t &calObj){
  
  // Retrieve etas2/phis2
  float etas2 = calObj.m_etas2;
  float phis2 = calObj.m_phis2;
  // Retrieve rectangular cell coordinates
  float cellX = calObj.m_x; 
  float cellY = calObj.m_y;
  float cellZ = calObj.m_z;
  // Calculate spherical cell coordinates
  double cellR     = sqrt( cellX*cellX + cellY*cellY + cellZ*cellZ);
  double cellphi   = TMath::ATan2( cellY, cellX );
  double celltheta = TMath::ACos( cellZ/cellR );
  double celleta   = -TMath::Log( TMath::Tan(celltheta/2) );
  // Calculate dPhi/dEta
  double deta = celleta - etas2; 
  double dphi = cellphi - phis2;
  if( dphi > TMath::Pi() )
    dphi = dphi - 2*TMath::Pi();
  else if ( dphi < -TMath::Pi() )
    dphi = dphi + 2*TMath::Pi();
  // Put dPhi/dEta in correct units
  dphi = dphi/0.0245; // 0.0245 units
  deta = deta/0.025;  // 0.025 units
  // Set the variables in calObj
  calObj.m_dphi = dphi;
  calObj.m_deta = deta;
 
  return EL::StatusCode::SUCCESS;
}


//
// Dump info from caloObject, for debugging
//__________________________________________________
void OfflineTimingTool::dumpInfo(caloObject_t &curr_obj){

    Info("OfflineTimingTool::dumpInfo", Form("\n RunNumber: %u\nRunNumber Index: %u\nIOV: %d\nOnline ID: %lu\nGain: %d\nEnergy: %f\nX: %f\nY: %f\nZ: %f\nEtas2: %f\nPhis2: %f\nf1: %f\nf3: %f\nPV_z: %f\ndphi: %f\ndeta: %f\n",
                curr_obj.m_rn,
                curr_obj.m_rn_ind, 
                curr_obj.m_iov, 
                curr_obj.m_onlId,
                curr_obj.m_gain,
                curr_obj.m_energy,
                curr_obj.m_x,
                curr_obj.m_y,
                curr_obj.m_z,
                curr_obj.m_etas2,
                curr_obj.m_phis2,
                curr_obj.m_f1,
                curr_obj.m_f3,
                curr_obj.m_PV_z,
                curr_obj.m_bunch_pos,
                curr_obj.valid,
                curr_obj.m_dphi,
                curr_obj.m_deta) );
    Info("OfflineTimingTool::dumpInfo",Form("\nFEB: %d\nFT: %d\nSlot: %d\nCh: %d",
                getFebNo(curr_obj.m_onlId),
                getFtNo(curr_obj.m_onlId),
                getSlotNo(curr_obj.m_onlId),
                getSubslotNo(curr_obj.m_onlId),
                getChannelNo(curr_obj.m_onlId)) );
  
}
