#include <OfflineTimingTool/OfflineTimingTool.h>

//
// Apply offline timing corrections
//________________________________
double OfflineTimingTool::applyCorrections(caloObject_t &curr_obj){

  // Print the variables read from container
  if( m_debug ) dumpInfo( curr_obj );

  
  if( m_debug )
    Info( "OfflineTimingTool::getCorrectedTime()", Form("Initial Time: t = %f, t_corr = %f",curr_obj.m_time, curr_obj.m_corrected_time));

  // Set valid flag.
  if( Set_valid( curr_obj ) != EL::StatusCode::SUCCESS ){
    Info( "OfflineTimingTool::getCorrectedTime()", "Failed valid flag setting");  
    return curr_obj.m_time;
  }


  // Apply time of flight correction
  if( tofCorrection( curr_obj ) != EL::StatusCode::SUCCESS ){
    Info( "OfflineTimingTool::getCorrectedTime()", "Failed TOF Correction");  
    return curr_obj.m_time;
  }
  
  if( m_debug )
    Info( "OfflineTimingTool::getCorrectedTime()", Form("After TOF: t_corr = %f", curr_obj.m_corrected_time));

  // Apply avg FT time per run corr
  if( applyPass0Corr( curr_obj ) != EL::StatusCode::SUCCESS ){
    Info( "OfflineTimingTool::getCorrectedTime()", "Failed Avg FT Correction");  
    return curr_obj.m_time;
  }
  
  if( m_debug )
    Info( "OfflineTimingTool::getCorrectedTime()", Form("After FT: t_corr = %f", curr_obj.m_corrected_time));

  // Apply avg FEB time corr
  if( applyPass1Corr( curr_obj ) != EL::StatusCode::SUCCESS ){
    Info( "OfflineTimingTool::getCorrectedTime()", "Failed Avg FEB Correction");  
    return curr_obj.m_time;
  }

  if( m_debug )
    Info( "OfflineTimingTool::getCorrectedTime()", Form("After FEB: t_corr = %f", curr_obj.m_corrected_time));
  
  // Apply avg ch time corr
  if( applyPass2Corr( curr_obj ) != EL::StatusCode::SUCCESS ){
    Info( "OfflineTimingTool::getCorrectedTime()", "Failed Avg Ch Correction (pass2)");  
    return curr_obj.m_time;
  }

  if( m_debug )
    Info( "OfflineTimingTool::getCorrectedTime()", Form("After Ch: t_corr = %f", curr_obj.m_corrected_time));
  
  // Apply avg energy time corr
  if( applyPass3Corr( curr_obj ) != EL::StatusCode::SUCCESS ){
    Info( "OfflineTimingTool::getCorrectedTime()", "Failed Avg Energy Correction");  
    return curr_obj.m_time;
  }

  if( m_debug )
    Info( "OfflineTimingTool::getCorrectedTime()", Form("After Energy: t_corr = %f", curr_obj.m_corrected_time));
  
  // Apply angular cell position time corr
  if( applyPass4Corr( curr_obj ) != EL::StatusCode::SUCCESS ){
    Info( "OfflineTimingTool::getCorrectedTime()", "Failed dphi/deta Correction");  
    return curr_obj.m_time;
  }

  if( m_debug )
    Info( "OfflineTimingTool::getCorrectedTime()", Form("After dphi/deta: t_corr = %f", curr_obj.m_corrected_time));
  
  // Apply energy frac time corr
  if( applyPass5Corr( curr_obj ) != EL::StatusCode::SUCCESS ){
    Info( "OfflineTimingTool::getCorrectedTime()", "Failed f1/f3 Correction");  
    return curr_obj.m_time;
  }

  if( m_debug )
    Info( "OfflineTimingTool::getCorrectedTime()", Form("After f1/f3: t_corr = %f", curr_obj.m_corrected_time));
  
  // Apply Bunch Positionc time corr
  if( applyPass6Corr( curr_obj ) != EL::StatusCode::SUCCESS ){
    Info( "OfflineTimingTool::getCorrectedTime()", "Failed Bunch position Correction");  
    return curr_obj.m_time;
  }

  if( m_debug )
    Info( "OfflineTimingTool::getCorrectedTime()", Form("After Bunch position: t_corr = %f", curr_obj.m_corrected_time));

  // Apply Avg Channel Time
  if( applyPass7Corr( curr_obj ) != EL::StatusCode::SUCCESS ){
    Info( "OfflineTimingTool::getCorrectedTime()", "Failed Avg Ch Correction (pass7) ");  
    return curr_obj.m_time;
  }
//  }


  // Apply avg FT time per run corr
  if( applyPass8Corr( curr_obj ) != EL::StatusCode::SUCCESS ){
    Info( "OfflineTimingTool::getCorrectedTime()", "Failed Avg FT Correction");  
    return curr_obj.m_time;
  }
  
  if( m_debug )
    Info( "OfflineTimingTool::getCorrectedTime()", Form("After 2nd FT: t_corr = %f", curr_obj.m_corrected_time));
  if( !curr_obj.valid ) curr_obj.m_corrected_time = -99999;

  return curr_obj.m_corrected_time;

}


//
// Apply Smearing to MC
//_______________________________________________________
double OfflineTimingTool::applySmearing(caloObject_t &calObj, double &t_coll, bool correlate ){

  // Get Slot to find correct resolution param
  double t_raw = calObj.m_corrected_time; // TOF correction only  
  double en    = calObj.m_energy;
  int gain     = calObj.m_gain;
  int slot     = getSlotNo( calObj.m_onlId ); 
  if( slot < 0 ){
    if( m_debug ) Info("OfflineTimingTool::applySmearing", Form("Warning! Unexpected slot, returning unsmeared time") );
    return calObj.m_corrected_time;
  }
  
  // Get resolution params based on slot/energy
  // [slot][0] slot index [slot][1-2] p0-p1
  double p0, p1;
  if( gain == 0 ){
    p0 = std::stod( resParamsH[slot][0+1] );
    p1 = std::stod( resParamsH[slot][1+1] );
  }else if( gain == 1){
    p0 = std::stod( resParamsM[slot][0+1] );
    p1 = std::stod( resParamsM[slot][1+1] );
  }else if( gain == 2){
    p0 = std::stod( resParamsL[slot][0+1] );
    p1 = std::stod( resParamsL[slot][1+1] );
  }else{
    if( m_debug ) Info("OfflineTimingTool::applySmearing", Form("Warning! Unexpected gain, returning unsmeared time") );
    return calObj.m_corrected_time;
  }
  
  // Calculate square of total sigma
  double sig_tot2 = (p0/en)*(p0/en) + p1*p1 -
                    m_sig_prompt_MC*m_sig_prompt_MC -
                    m_sig_beamSpread*m_sig_beamSpread;
  // Make sure above zero (possible rounding errors)
  if( sig_tot2 < 0. ) sig_tot2 = 0.;
  // Take sqrt
  sig_tot2 = sqrt( sig_tot2 );
  
  // Seed random for smearing
  TRandom3 *m_random = new TRandom3();
  m_random->SetSeed(0);
  
  // For first electron, set collision time (second electron uses this value)
  if( !correlate ) 
    t_coll = m_random->Gaus(0, m_sig_beamSpread);

  double t_smear = t_raw + t_coll + m_random->Gaus(0, sig_tot2);

  delete m_random;

  return t_smear;
}


EL::StatusCode OfflineTimingTool::Set_valid(caloObject_t &calObj){

  if(calObj.m_gain == 2 ){
	  calObj.valid = false;
    Info( "OfflineTimingTool::getCorrectedTime()", Form("The corrected time is invalid, as we don't want LOW GAIN event!"));
      return EL::StatusCode::SUCCESS; 
   } 
   if(calObj.m_energy < 5.0 ){
    Info( "OfflineTimingTool::getCorrectedTime()", Form("The corrected time is invalid, as maxcell energy is below 5GeV!"));
	  calObj.valid = false;
      return EL::StatusCode::SUCCESS; 
  }
 
  if(fabs(calObj.m_eta) > 2.47 || (fabs(calObj.m_eta) > 1.37 && fabs(calObj.m_eta) < 1.52)){
    Info( "OfflineTimingTool::getCorrectedTime()", Form("The corrected time is invalid, as not in range of EMB or EMEC!"));
	  calObj.valid = false;
      return EL::StatusCode::SUCCESS; 
      }
  int ch   = getChannelNo( calObj.m_onlId );
  if( ch < 0 ) return EL::StatusCode::FAILURE;

  unsigned int bad_ch = std::find( bad_channelList.begin(), bad_channelList.end(), ch) - bad_channelList.begin();                  
  if(bad_ch>=0 && bad_ch <bad_channelList.size()){
    Info( "OfflineTimingTool::getCorrectedTime()", Form("The corrected time is invalid, as it's from a bad channel!"));
	  calObj.valid = false;
      calObj.m_corrected_time = -99999;
      return EL::StatusCode::SUCCESS;
  }
   
  calObj.valid = true;  
  return EL::StatusCode::SUCCESS;
}
//
// Apply TOF correction
//________________________________________________________
EL::StatusCode OfflineTimingTool::tofCorrection(caloObject_t &calObj){
  
  // MaxEnergy Cell info
  double x = calObj.m_x; 
  double y = calObj.m_y;
  double z = calObj.m_z;
  double t = calObj.m_time;

  // Primary vertex z
  double PV_z = calObj.m_PV_z;
  
  // Speed of light in mm/ns
  const double c_mmns = 299.792458;
  
  // Distances for cell and Cell minus PV_z
  double r_cell = sqrt( x*x + y*y + z*z);
  double r_path = sqrt( x*x + y*y + (z - PV_z)*(z - PV_z));

  // TOF correction
  double t_tof = (r_path - r_cell)/c_mmns;

  // Save correction
  calObj.m_corrected_time =  t-t_tof;
  // should this be added not subtracted??

  return EL::StatusCode::SUCCESS;
}


//
// Apply run by run avg FT corrections
//_______________________________________________________
EL::StatusCode OfflineTimingTool::applyPass0Corr(caloObject_t &calObj){

  // Get FT number/gain
  int ft   = getFtNo( calObj.m_onlId );
  int gain = calObj.m_gain;
  //  = calObj.m_iov;
  unsigned int pos = calObj.m_rn_ind; // run index

  if( ft < 0 ) return EL::StatusCode::FAILURE;

  // [ft][0] = ft index;  [ft][1-n] = run avg FT time
  double dt = (gain == 0 ? std::stod( p0CorrH[ft][1+pos] ) : 
                           std::stod( p0CorrM[ft][1+pos] ) );
  if(m_debug )
  cout<<ft<<"\t"<<calObj.m_rn_ind<<endl;
  calObj.m_corrected_time -= dt; 

  return EL::StatusCode::SUCCESS;
}


//
// Apply avg FEB correcctions
//________________________________________________________
EL::StatusCode OfflineTimingTool::applyPass1Corr(caloObject_t &calObj){

  // Get FEB/gain
  int feb  = getFebNo( calObj.m_onlId );
  int gain = calObj.m_gain;
  //  = calObj.m_iov;
  
  if( feb < 0 ) return EL::StatusCode::FAILURE;

  // Calculate time correction
  double dt = (gain == 0 ? std::stod( p1CorrH[feb] ) : 
                           std::stod( p1CorrM[feb] ) );
  calObj.m_corrected_time -= dt;

  return EL::StatusCode::SUCCESS;
}


//
// Apply the channel by channel avg corrections
//_______________________________________________________
EL::StatusCode OfflineTimingTool::applyPass2Corr(caloObject_t &calObj){

  // Get ch/gain
  int ch   = getChannelNo( calObj.m_onlId );
  int gain = calObj.m_gain;
  int iov  = calObj.m_iov;

  
//  cout<<iov<<"\t"<<ch <<"\t"<<gain<<endl;
  if( ch < 0 ) return EL::StatusCode::FAILURE;

  // Calculate time correction
  double dt = (gain == 0 ? std::stod( p2CorrH[ch][iov+1] ) :
                           std::stod( p2CorrM[ch][iov+1] ) );
  calObj.m_corrected_time -= dt; 
  if(m_debug)
  cout<<iov<<"\t"<<ch <<"\t"<<p2CorrH[ch][iov]<<"\t"<<gain<<endl;
  unsigned int bad_ch = std::find( bad_channelList.begin(), bad_channelList.end(), ch) - bad_channelList.begin();                  
  if(bad_ch>=0 && bad_ch <bad_channelList.size()){
	  calObj.valid = false;
      calObj.m_corrected_time = -99999;
  }
  return EL::StatusCode::SUCCESS;
}

//
// Apply the energy avg time corrections
//_________________________________________________________
EL::StatusCode OfflineTimingTool::applyPass3Corr(caloObject_t &calObj){

  // Get En/Slot/Gain
  double en = calObj.m_energy;
  int gain  = calObj.m_gain;
  int slot;
  if(gain==0)
   slot  = getSubslotNo( calObj.m_onlId );
  else 
	  slot = getSlotNo(calObj.m_onlId);
  
  //   = calObj.m_iov;
//
  if( slot < 0 ) return EL::StatusCode::FAILURE;

  
  // [slot][0] slot index [slot][1-2] fit range [slot][3-8] p0-p6 fit params
  double minX = (gain == 0 ? std::stod( p3CorrH[slot][0+1] ) : 
                             std::stod( p3CorrM[slot][0+1] ) );
  double bond = (gain == 0 ? std::stod( p3CorrH[slot][0+1+1] ) : 
                             std::stod( p3CorrM[slot][0+1+1] ) );
  double maxX = (gain == 0 ? std::stod( p3CorrH[slot][1+1+1] ) : 
                             std::stod( p3CorrM[slot][1+1+1] ) );

  double p0 = (gain == 0 ? std::stod( p3CorrH[slot][0+2+2] ) : 
                           std::stod( p3CorrM[slot][0+2+2] ) );
  double p1 = (gain == 0 ? std::stod( p3CorrH[slot][1+2+2] ) : 
                           std::stod( p3CorrM[slot][1+2+2] ) );
  double p2 = (gain == 0 ? std::stod( p3CorrH[slot][2+2+2] ) : 
                           std::stod( p3CorrM[slot][2+2+2] ) );
  double p3 = (gain == 0 ? std::stod( p3CorrH[slot][3+2+2] ) : 
                           std::stod( p3CorrM[slot][3+2+2] ) );
  double p4 = (gain == 0 ? std::stod( p3CorrH[slot][4+2+2] ) : 
                           std::stod( p3CorrM[slot][4+2+2] ) );
  double p5 = (gain == 0 ? std::stod( p3CorrH[slot][5+2+2] ) : 
                           std::stod( p3CorrM[slot][5+2+2] ) );
  double p6, p7, p8, p9;

  if(gain==1){
	  p6 =  std::stod( p3CorrM[slot][2+2+2+4] );
	  p7 =  std::stod( p3CorrM[slot][3+2+2+4] );
	  p8 =  std::stod( p3CorrM[slot][4+2+2+4] );
	  p9 =  std::stod( p3CorrM[slot][5+2+2+4] );
  }

  // Constant correction outside range of fit
  if( en < minX ) en = minX;
  if( en > maxX ) en = maxX;

  
  double dt;
  if(gain==0)
	  dt = p0 + p1*en + p2*en*en + p3*en*en*en + p4*en*en*en*en + p5*en*en*en*en*en;
  if(gain==1){
	  if(en<=bond){
		  dt = p0 + p1*en + p2*en*en + p3*en*en*en + p4*en*en*en*en + p5*en*en*en*en*en;
	  }
	  else{
		  dt = p6 + p7*en + p8*en*en + p9*en*en*en; 
	  }
  }
  calObj.m_corrected_time -= dt; 

  return EL::StatusCode::SUCCESS;
}

//
// Apply the dphi/deta corrections
//_______________________________________________________
EL::StatusCode OfflineTimingTool::applyPass4Corr(caloObject_t &calObj){

  // Get dPhi/dEta/Slot/Gain
  double dphi = calObj.m_dphi;
  double deta = calObj.m_deta;
  int slot    = getSlotNo( calObj.m_onlId );
  int gain    = calObj.m_gain;
  //     = calObj.m_iov;

  if( slot < 0 ) return EL::StatusCode::FAILURE;

  // [slot][0] slot index [slot][1-5] dphi fit params
  double pminX = (gain == 0 ? std::stod( p4CorrH[slot][0+1] ) : 
                              std::stod( p4CorrM[slot][0+1] ) );
  double pmaxX = (gain == 0 ? std::stod( p4CorrH[slot][1+1] ) : 
                              std::stod( p4CorrM[slot][1+1] ) );   
  double p0 = (gain == 0 ? std::stod( p4CorrH[slot][0+1+4] ) : 
                           std::stod( p4CorrM[slot][0+1+4] ) );
  double p1 = (gain == 0 ? std::stod( p4CorrH[slot][1+1+4] ) : 
                           std::stod( p4CorrM[slot][1+1+4] ) );
  double p2 = (gain == 0 ? std::stod( p4CorrH[slot][2+1+4] ) : 
                           std::stod( p4CorrM[slot][2+1+4] ) );
  double p3 = (gain == 0 ? std::stod( p4CorrH[slot][3+1+4] ) :
                           std::stod( p4CorrM[slot][3+1+4] ) );
  double p4 = (gain == 0 ? std::stod( p4CorrH[slot][4+1+4] ) : 
                           std::stod( p4CorrM[slot][4+1+4] ) );
  // [slot][0] slot index [slot][6-10] deta fit params
  double eminX = (gain == 0 ? std::stod( p4CorrH[slot][0+1+2] ) : 
                              std::stod( p4CorrM[slot][0+1+2] ) );
  double emaxX = (gain == 0 ? std::stod( p4CorrH[slot][1+1+2] ) : 
                              std::stod( p4CorrM[slot][1+1+2] ) );
  double e0 = (gain == 0 ? std::stod( p4CorrH[slot][0+1+4+5] ) : 
                           std::stod( p4CorrM[slot][0+1+4+5] ) );
  double e1 = (gain == 0 ? std::stod( p4CorrH[slot][1+1+4+5] ) : 
                           std::stod( p4CorrM[slot][1+1+4+5] ) );
  double e2 = (gain == 0 ? std::stod( p4CorrH[slot][2+1+4+5] ) : 
                           std::stod( p4CorrM[slot][2+1+4+5] ) );
  double e3 = (gain == 0 ? std::stod( p4CorrH[slot][3+1+4+5] ) : 
                           std::stod( p4CorrM[slot][3+1+4+5] ) );
  double e4 = (gain == 0 ? std::stod( p4CorrH[slot][4+1+4+5] ) : 
                           std::stod( p4CorrM[slot][4+1+4+5] ) );

  // Correction
  double dtp = p0 + p1*dphi + p2*dphi*dphi + p3*dphi*dphi*dphi + p4*dphi*dphi*dphi*dphi;
  double dte = e0 + e1*deta + e2*deta*deta + e3*deta*deta*deta + e4*deta*deta*deta*deta;
  // Constant correction outside the fit range
  if( dphi < -0.5 ) dtp = pminX;
  if( dphi > 0.5 )  dtp = pmaxX;
  if( deta < -0.5 ) dte = eminX;
  if( deta > 0.5 )  dte = emaxX;

  calObj.m_corrected_time -= (dtp + dte); 

  return EL::StatusCode::SUCCESS;
}


//
// Apply the energy frac corrections
//_______________________________________________________
EL::StatusCode OfflineTimingTool::applyPass5Corr(caloObject_t &calObj){

  // Get f1/f3/slot/gain
  double f1 = calObj.m_f1;
  double f3 = calObj.m_f3;
  int slot  = getSlotNo( calObj.m_onlId );
  int gain  = calObj.m_gain;
  //   = calObj.m_iov;

  if( slot < 0 ) return EL::StatusCode::FAILURE;

  // [slot][0] slot index [slot][1-2] f1 fit params
  double f1_min = (gain == 0 ? std::stod( p5CorrH[slot][0+1] ) : 
                               std::stod( p5CorrM[slot][0+1] ) );
  double f1_max = (gain == 0 ? std::stod( p5CorrH[slot][1+1] ) : 
                               std::stod( p5CorrM[slot][1+1] ) );
  double f1_p0  = (gain == 0 ? std::stod( p5CorrH[slot][0+1+4] ) : 
                               std::stod( p5CorrM[slot][0+1+4] ) );
  double f1_p1  = (gain == 0 ? std::stod( p5CorrH[slot][1+1+4] ) : 
                               std::stod( p5CorrM[slot][1+1+4] ) );
  // [slot][0] slot index [slot][3-4] f3 fit params
  double f3_min = (gain == 0 ? std::stod( p5CorrH[slot][0+1+2] ) : 
                               std::stod( p5CorrM[slot][0+1+2] ) );
  double f3_max = (gain == 0 ? std::stod( p5CorrH[slot][1+1+2] ) : 
                               std::stod( p5CorrM[slot][1+1+2] ) );
  double f3_p0  = (gain == 0 ? std::stod( p5CorrH[slot][0+1+4+2] ) : 
                               std::stod( p5CorrM[slot][0+1+4+2] ) );
  double f3_p1  = (gain == 0 ? std::stod( p5CorrH[slot][1+1+4+2] ) : 
                               std::stod( p5CorrM[slot][1+1+4+2] ) );

  // constant correciton outside fit range
  if( f1 < f1_min ) f1 = f1_min;
  if( f1 > f1_max ) f1 = f1_max;
  if( f3 < f3_min ) f3 = f3_min;
  if( f3 > f3_max ) f3 = f3_max;

  double dt1 = f1_p0 + f1_p1*f1;
  double dt3 = f3_p0 + f3_p1*f3;
  calObj.m_corrected_time -= (dt1 + dt3); 

  return EL::StatusCode::SUCCESS;
}

//
// Apply the Bunch position  corrections
//_______________________________________________________
EL::StatusCode OfflineTimingTool::applyPass6Corr(caloObject_t &calObj){
	if( calObj.m_bunch_pos>=3000) 
		calObj.m_corrected_time -= 0;
	else{
		// Get bp/Slot/Gain
		int bp = calObj.m_bunch_pos/25;
		int slot  = getSlotNo( calObj.m_onlId );
		int gain  = calObj.m_gain;
		//   = calObj.m_iov;

	        if( slot < 0 ) return EL::StatusCode::FAILURE;
		double dt;

		if (slot !=8 || slot != 9 ) 
			dt = (gain == 0 ? std::stod( p6CorrH[slot][1+bp] ) : 
					std::stod( p6CorrM[slot][1+bp] ) );

		if(m_debug)
			cout<<"iamhere"<<dt<<endl;
		calObj.m_corrected_time -= dt; 
		//    if(m_debug)
		//	    cout<<"DEBUG!"<< calObj.m_bunch_pos<<"\t"<<dt<<"\t"<<gain<<"\t"<<slot<<endl;

	}
  return EL::StatusCode::SUCCESS;
}
//
// Apply the channel by channel avg corrections
//_______________________________________________________
EL::StatusCode OfflineTimingTool::applyPass7Corr(caloObject_t &calObj){

  // Get ch/gain
  int ch   = getChannelNo( calObj.m_onlId );
  int gain = calObj.m_gain;
  //  = calObj.m_iov;

  if( ch < 0 ) return EL::StatusCode::FAILURE;

  double dt = (gain == 0 ? std::stod( p7CorrH[ch] ) :
                           std::stod( p7CorrM[ch] ) );
  calObj.m_corrected_time -= dt; 

  return EL::StatusCode::SUCCESS;
}


//
// Apply 2nd run by run avg FT corrections
//_______________________________________________________
EL::StatusCode OfflineTimingTool::applyPass8Corr(caloObject_t &calObj){

  // Get FT number/gain
  int ft   = getFtNo( calObj.m_onlId );
  int gain = calObj.m_gain;
  //  = calObj.m_iov;
  unsigned int pos = calObj.m_rn_ind; // run index

  if( ft < 0 ) return EL::StatusCode::FAILURE;

  // [ft][0] = ft index;  [ft][1-n] = run avg FT time
  double dt = (gain == 0 ? std::stod( p8CorrH[ft][1+pos] ) : 
                           std::stod( p8CorrM[ft][1+pos] ) );
  if(m_debug )
  cout<<ft<<"\t"<<calObj.m_rn_ind<<endl;
  calObj.m_corrected_time -= dt; 

  return EL::StatusCode::SUCCESS;
}


