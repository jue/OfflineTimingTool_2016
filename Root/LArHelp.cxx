#include <OfflineTimingTool/OfflineTimingTool.h>

//
// Returns FeedThrough index given the Online ID
//_____________________________________________
int OfflineTimingTool::getFtNo(unsigned long int onlId){

  // 114 FT indicies total 
  // 32 x 2 (A/C) = 64 EMB
  // 25 x 2 (A/C) = 50 EMEC
  
  if( onlId >> 26 != 14){
    Info( "OfflineTimingTool::getFtNo", Form("Not a valid LAr Online Id:%lx",onlId) );
    return -1;
  }

  int pn   = ( onlId >> 24 )&0x1;  // positive (1=A)/negative(0=C)
  int ft   = ( onlId >> 19 )&0x1F; // feedthrough
  int benc = ( onlId >> 25 )&0x1;  // barrel(0)/endcap(1)
  
  if( ft < 0 || ft > 31){
    Info( "OfflineTimingTool::getFtNo", Form("Warning! Unexpected FT: %d (Should be 0-31)",ft));
    return -1;
  }

  int bin = ft;

  if(!pn) bin = ft + 32;

  if(benc){
    bin = 64 + ft;
    if(!pn) bin = bin + 25;
  }

  return bin;
}

//
// Returns the Slot index given the Online ID
//_______________________________________________
int OfflineTimingTool::getSlotNo(unsigned long int onlId){
  
  // 22 Slot indicies in total
  // EMB: Sl 10-14 (5) x 2 (A/C) = 10
  // EMEC: Sl 10-15 (6) x 2 (A/C) = 12
  // Slot index to actual slot conversion below
  // EMB
  // Slot 11
  //   A: 0 C: 4
  // Slot 12
  //   A: 1 C: 5
  // Slot 13
  //   A: 2 C: 6
  // Slot 14
  //   A: 3 C: 7
  // Slot 10
  //   A: 8 C: 9
  // EMEC
  // Slot 10
  //   A: 10 C: 16
  // Slot 11
  //   A: 11 C: 17
  // Slot 12
  //   A: 12 C: 18
  // Slot 13
  //   A: 13 C: 19
  // Slot 14
  //   A: 14 C: 20
  // Slot 15
  //   A: 15 C: 21


  if( onlId >> 26 != 14){
    Info( "OfflineTimingTool::getSlotNo", "Not a valid LAr Online Id" );
    return -1;
  }

  int pn   = ( onlId >> 24 )&0x1;
  int sl   = ((onlId >> 15 )&0xF)+1;
  int benc = ( onlId >> 25 )&0x1;

//  if( sl < 10 || sl > 15){
//    Info( "OfflineTimingTool::getSlotNo", Form("Warning! Unexpected Slot: %d (Should be 10-15)",sl));
//    return -1;
//  }
	Info("slot", Form("%d, %d ,%d") , pn, sl, benc);
  if( sl == 10 && !benc){
    Info( "OfflineTimingTool::getSlotNo", Form("Warning! Unexpected EMB Slot: %d (Should be 11-15)",sl));
    return -1;
  }
  
  int bin = sl - 11;

  if(!pn) bin = bin + 4;
  
  if( sl == 10 ){
    bin = 8;
    if(!pn) bin = 9;
  }

  if(benc){
    bin = 10 + sl -10; 
    if(!pn) bin = bin + 6;
  }

  return bin;

}

int OfflineTimingTool::getSubslotNo(unsigned long int onlId){
	int channel = getChannelNo(onlId);
	int slot  = getSlotNo(onlId);   

	unsigned int ch = (unsigned int)channel;
	
	unsigned int chpos_subslot = std::find ( channelList_for_subslot.begin(),channelList_for_subslot.end(),ch) - channelList_for_subslot.begin();
	if( chpos_subslot>=0 && chpos_subslot<channelList_for_subslot.size() ){
		switch(slot){
			case 12: slot = 22;break;
			case 13: slot = 23;break;
			case 14: slot = 24;break;
			case 15: slot = 25;break;
			case 19: slot = 26;break;
			case 20: slot = 27;break;
			case 21: slot = 28;
		}
	}
	return slot;
}

//
// Returns the Front End Board Number given the Online ID
//______________________________________________________
int OfflineTimingTool::getFebNo(unsigned long int onlId){
 
  // 620 Febs in total
  // EMB:  Sl 10-14 (5) x A/C (2) x 32FT = 320
  // EMEC: Sl 10-15 (6) x A/C (2) X 25FT = 300
  if( onlId >> 26 != 14){
    Info( "OfflineTimingTool::getFebNo", "Not a valid LAr Online Id" );
    return -1;
  }

  int pn   = ( onlId >> 24 )&0x1;
  int ft   = ( onlId >> 19 )&0x1F;
  int sl   = ((onlId >> 15 )&0xF)+1;
  int benc = ( onlId >> 25 )&0x1;
  
  if( ft < 0 || ft > 31){
    Info( "OfflineTimingTool::getFebNo", Form("Warning! Unexpected FT: %d (Should be 0-31)",sl));
    return -1;
  }

  int bin = (sl - 11)*32 + ft;
  
  if(!pn) bin = bin + 128;

  if( sl == 10){
    bin = 256 + ft;
    if(!pn) bin = bin + 32;
  }

  if(benc){
    bin = 256 + 64 + (sl - 10)*25 + ft; 
    if(!pn) bin = bin + 6*25;  
  }

  return bin;

}

//
// Returns channel index given the Online ID
//_____________________________________________________
int OfflineTimingTool::getChannelNo(unsigned long int onlId){

  // 620 Febs x 128 ch/feb = 79360 ch
 
  int fn = getFebNo(onlId);
  int ch = (onlId >> 8)&0x7F;
  int bin = fn * 128 + ch;  
  
  if( ch < 0 || ch > 127){
    Info( "OfflineTimingTool::getChNo", Form("Warning! Unexpected Channel: %d (Should be 0-127)",ch));
    return -1;
  }
  
  return bin;
}

