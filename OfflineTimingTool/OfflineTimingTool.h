//________________________________
//
// Offline Precision Timing Tool
//
// Author: Ryne Carbone
// Date:   Jan 2016
// Email   rc2809@columbia.edu
//         ryne.carbone@cern.ch
//________________________________
// Updates since Oct 2016:   Jue Chen
// Email:         jue@cern.ch

#ifndef OFFLINETIMINGTOOL_H
#define OFFLINETIMINGTOOL_H

// Event Loop Includes
#include <EventLoop/Job.h>
#include <EventLoop/StatusCode.h>
#include <EventLoop/Worker.h>
#include <EventLoop/Algorithm.h>

// xAOD Includes
#include "xAODRootAccess/Init.h"
#include "xAODRootAccess/TEvent.h"
#include "xAODRootAccess/TStore.h"
#include "xAODRootAccess/tools/Message.h"

// Event Info Includes
#include "xAODEventInfo/EventInfo.h"

// Egamma Includes
#include "xAODEgamma/ElectronContainer.h"
#include "xAODEgamma/ElectronAuxContainer.h"
#include "xAODEgamma/PhotonContainer.h"
#include "xAODEgamma/PhotonAuxContainer.h"

//Vertex Container
#include "xAODTracking/VertexContainer.h"
#include "xAODTracking/VertexAuxContainer.h"
#include "xAODTracking/Vertex.h"
#include <tuple>
// PathResolver
#include "PathResolver/PathResolver.h"

// General Includes
#include <iostream>
#include <fstream>
#include "TFile.h"
#include "TMath.h"
#include "TRandom3.h"
#include "boost/algorithm/string.hpp"
//bucnh crossing
#include "TrigAnalysisInterfaces/IBunchCrossingTool.h"   
#include "TrigBunchCrossingTool/WebBunchCrossingTool.h"
#include "TrigBunchCrossingTool/StaticBunchCrossingTool.h"
 using namespace std;  


class OfflineTimingTool{

  private:
    // Variables need to compute offline correction
    struct caloObject_t{
      unsigned long int m_onlId;  // LAr Online Id 
      unsigned int      m_rn;     // Run number    
      unsigned int      m_rn_ind; // Run index in IOV
      int               m_iov;    // IOV number
      int               m_gain;   // Max Ecell Gain
      float             m_energy; // Max Ecell Energy(store in GeV!)
      float             m_time;   // Max Ecell Time
      float             m_x;      // Max Ecell X
      float             m_y;      // Max Ecell Y
      float             m_z;      // Max Ecell Z
      float             m_eta;      // Cell eta
      float             m_etas2;  // Etas2 from calocluster
      float             m_phis2;  // Phis2 from calocluster
      float             m_f1;     // Fraction of energy in layer 1
      float             m_f3;     // Fraction of energy in layer 3
      float             m_PV_z;   // Primary Vertex Z
      double            m_dphi;   // Dif in phi from barycenter of calClust & cell
      double            m_deta;   // Dif in eta from barycenter of calClust & cell
      int   	        m_bunch_pos; //event position in bunch train
      double            m_corrected_time; // Corrected time
      bool 	        	valid; 		// flag the correction is valid
    };

    typedef struct caloObject_t caloObject_t;

    // In LArHelp.cxx, HW information
    int getChannelNo(unsigned long int);
    int getSlotNo   (unsigned long int);
    int getSubslotNo   (unsigned long int);
    int getFebNo    (unsigned long int);
    int getFtNo     (unsigned long int);
    int getSubslot  (unsigned long int);   
  
    const double GeV = 1000.; //! convert MeV to GeV
    bool m_debug;             //! more verbose printout

  
    // Set the local variables for calo object
    template <class T> 
    OfflineTimingTool::caloObject_t setCaloObject(T calObj, unsigned int rn, float PV_z, int bunch_pos){ 
      // calo Object to return
      caloObject_t return_obj;
      return_obj.m_rn     = rn;
      return_obj.m_bunch_pos   = bunch_pos;
      return_obj.m_onlId  = calObj.template auxdata<unsigned long int>("maxEcell_onlId");
      return_obj.m_onlId  = return_obj.m_onlId >> 32; //Is stored as 64bit, last 32 bits area zero
      return_obj.m_gain   = calObj.template auxdata<int>("maxEcell_gain");
      return_obj.m_energy = calObj.template auxdata<float>("maxEcell_energy")/GeV;
      return_obj.m_time   = calObj.template auxdata<float>("maxEcell_time");
      return_obj.m_x      = calObj.template auxdata<float>("maxEcell_x");
      return_obj.m_y      = calObj.template auxdata<float>("maxEcell_y");
      return_obj.m_z      = calObj.template auxdata<float>("maxEcell_z");
      return_obj.m_f1     = calObj.template auxdata<float>("f1");
      return_obj.m_f3     = calObj.template auxdata<float>("f3");
      return_obj.m_eta    = calObj.caloCluster()->eta();   
      // Calo Cluster info
      return_obj.m_etas2  = calObj.caloCluster()->etaBE(2);
      return_obj.m_phis2  = calObj.caloCluster()->phiBE(2);
      return_obj.m_PV_z   = PV_z;
      // Time to keep track of corrections
      return_obj.m_corrected_time = calObj.template auxdata<float>("maxEcell_time");
      return_obj.valid    =true;
      return return_obj;
    }

    // Print out information (for debugging)
    void dumpInfo              (caloObject_t &calObj);

    // Primary Vertex Z / Angular Cell Position
    EL::StatusCode setIOV      (caloObject_t &calObj);
    EL::StatusCode setdPhidEta (caloObject_t &calObj);
    EL::StatusCode setBunchPosition(caloObject_t &calObj);


    // Apply each correction

    double       applyCorrections(caloObject_t &calObj);
    EL::StatusCode Set_valid(caloObject_t &calObj);
    EL::StatusCode tofCorrection (caloObject_t &calObj); // Time of Flight
    EL::StatusCode applyPass0Corr(caloObject_t &calObj); // Run by Run FT time
    EL::StatusCode applyPass1Corr(caloObject_t &calObj); // Avg FEB time
    EL::StatusCode applyPass2Corr(caloObject_t &calObj); // Avg Ch time
    EL::StatusCode applyPass3Corr(caloObject_t &calObj); // Avg En correction
    EL::StatusCode applyPass4Corr(caloObject_t &calObj); // dphi/deta correction
    EL::StatusCode applyPass5Corr(caloObject_t &calObj); // f1/f3 correction
    EL::StatusCode applyPass6Corr(caloObject_t &calObj); // bunch position correction
    EL::StatusCode applyPass7Corr(caloObject_t &calObj); // Avg Ch time
    EL::StatusCode applyPass8Corr(caloObject_t &calObj); // Run by Run FT time
    
    // Apply smearing
    double applySmearing(caloObject_t &calObj, double &t_coll, bool correlate);

    // Vectors to store corrections
    // Key: IOV# -- Value: RunNumber
    std::vector<int> 		      iovNumberList; //! 
    std::vector<int>                  runNumberList; //!
    std::vector<int>                  channelList_for_subslot; //!
    std::vector<int>                  bad_channelList; //!
    // Key: IOV# -- Value: Vector FT indicies, with vector of corr by run#
    std::vector< std::vector<std::string> > p0CorrH; //!
    std::vector< std::vector<std::string> > p0CorrM; //!
    std::vector< std::vector<std::string> > p8CorrH; //!
    std::vector< std::vector<std::string> > p8CorrM; //!
    // Key: IOV# -- Value: Vector of FEB corr 
    std::vector< std::string >              p1CorrH; //!
    std::vector< std::string >              p1CorrM; //!
    // Key: IOV# -- Value: Vector of Channel corr 
    std::vector< std::vector<std::string> > p2CorrH; //!
    std::vector< std::vector<std::string> > p2CorrM; //!
    // Key: IOV# -- Value: Vector Slot indicies, with vector of fit params
    std::vector< std::vector<std::string> > p3CorrH; //!
    std::vector< std::vector<std::string> > p3CorrM; //!
    std::vector< std::vector<std::string> > p4CorrH; //!
    std::vector< std::vector<std::string> > p4CorrM; //!
    std::vector< std::vector<std::string> > p5CorrH; //!
    std::vector< std::vector<std::string> > p5CorrM; //!
    std::vector< std::vector<std::string> > p6CorrH; //!
    std::vector< std::vector<std::string> > p6CorrM; //!
    // Key: IOV# -- Value: Vector of Channel corr 
    std::vector< std::string >              p7CorrH; //!
    std::vector< std::string >              p7CorrM; //!

    // For smearing MC times
    // Vector Slot indicies with vector of p0, p1 res params
    std::vector< std::vector<std::string> > resParamsH; //!
    std::vector< std::vector<std::string> > resParamsM; //!
    std::vector< std::vector<std::string> > resParamsL; //!
    double m_sig_beamSpread = 0.200; 
    double m_sig_prompt_MC  = 0.130; 

  public:
    // Constructor
    OfflineTimingTool(bool debug=false);
    // Initialize/Load corrections
    EL::StatusCode initialize();

    // Calculate offline correction
    template <class T> 
	    std::tuple<bool, double>getCorrectedTime(T calObj, unsigned int rn, float PV_z, int m_bunch_pos){
		    // Set the local variables for this calo object
		    caloObject_t curr_obj = setCaloObject<T>( calObj, rn, PV_z ,m_bunch_pos);
		    // Set the IOV
		    if( setIOV( curr_obj ) != EL::StatusCode::SUCCESS ){
			    if( m_debug ) Info("OfflineTimingTool::getCorrectedTime", Form("Warning! Unable to set iov, returning raw time") );
			    return std::make_tuple(curr_obj.valid,  curr_obj.m_time);
		    }      
		    // Set dPhi/dEta
		    setdPhidEta( curr_obj );
		  //  setBunchPosition( curr_obj );
		    // Apply the corrections
		    double corrected_time = applyCorrections( curr_obj );
		    return std::make_tuple(curr_obj.valid, corrected_time);
	    }

    // Calculate MC smeared time
    // set correlate to true if using previous t_coll
    template <class T>
	    double getSmearedTime(T calObj, float PV_z, double &t_coll, bool correlate){
		    // Set local variable for this calo object (rn number is not needed, set to 0)
		    caloObject_t curr_obj = setCaloObject<T>( calObj, 0, PV_z,0);
		    // Calculate time of flight correction
		    if( tofCorrection( curr_obj ) != EL::StatusCode::SUCCESS ){
			    if( m_debug) Info("OfflineTimingtool::getSmearedTime", Form("Warning! unable to calculate TOF correction, returning raw time!") );
			    return curr_obj.m_time;
		    }
		    // Apply smearing
		    double t_smear = applySmearing( curr_obj, t_coll, correlate) ;
		    return t_smear;
	    }

    // Destructor
    ~OfflineTimingTool(){};

};



#endif
